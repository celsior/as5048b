#include "mbed.h"

DigitalOut myled(LED1);
Serial pc(USBTX, USBRX);


struct AS5048B
{
  int addr;
  I2C i2c;
  enum class Register {gain = 0xfa, diag = 0xfb, mag_hi = 0xfc, mag_lo = 0xfd, angle_hi = 0xfe, angle_lo = 0xff};

  struct Diag
  {
    int val;
    Diag(int val)
      : val(val) {};

    bool OCF() {return val & 0b1;}
    bool COF() {return val & 0b10;}
    bool low() {return val & 0b100;}
    bool high() {return val & 0b1000;}
  };

  AS5048B(int addr, PinName sda, PinName sdc)
    : addr(addr), i2c(sda, sdc) { i2c.frequency(400000);}

  bool read_register(Register reg, int* result)
  {
    char dt = (char)reg;
    int err = i2c.write(addr, &dt, 1, true);
    if (err)
      return false;

    err = i2c.read(addr, &dt, 1);
    if (err)
      return false;

    *result = dt;
    return true;
  }

  bool write_register(Register reg, int data)
  {
    char dt = (char)reg;
    int err = i2c.write(addr, &dt, 1, true);
    if (err)
      return false;
    return true;
  }

  int mag_raw()
  {
    int mag_lo, mag_hi;
    if (! read_register(Register::mag_lo, &mag_lo))
      return -1;
    if (! read_register(Register::mag_hi, &mag_hi))
      return -1;

    mag_hi &= 0b00111111;

    return ((unsigned int)mag_hi << 8) + (unsigned int)mag_lo;
  }

  int angle_raw()
  {
    int angle_lo, angle_hi;
    if (! read_register(Register::angle_lo, &angle_lo))
      return -1;
    if (! read_register(Register::angle_hi, &angle_hi))
      return -1;

    angle_hi &= 0b00111111;

    return ((unsigned int)angle_hi << 8) + (unsigned int)angle_lo;
  }

  double angle_rad()
  {
    return angle_raw() * 2*M_PI / 0x4000;
  }

  Diag diag()
  {
    int diag;
    if (! read_register(Register::diag, &diag))
      return -1;
    return Diag(diag);
  }

  int gain()
  {
    int gain;
    if (! read_register(Register::gain, &gain))
      return -1;
    return gain;
  }

};

AS5048B as5048b(0x80, I2C_SDA, I2C_SCL);


int main()
{
  int cnt = 0;
  myled = 0;
  pc.printf("%d\n", cnt);
  pc.printf("%d\n", cnt);

  pc.printf("start\n");


  // for (;;)
  //   {
  //     pc.printf("ping\n");
  //     as5048b.i2c.start();
  //     int ack = as5048b.i2c.write(0x80);
  //     if (ack)
  //       {
  //         //pc.printf("device %x %d\n", i << 1, ack);
  //         pc.printf("ack\n");
  //         as5048b.i2c.stop();
  //       }
  //     wait(0.2);
  //   }

  // return 0;

  for(int i = 0;; ++i)
    {
      double angle = as5048b.angle_rad() / M_PI * 180;
      int gain = as5048b.gain();
      auto diag = as5048b.diag();

      double avg_angle = 0;
      for (int k = 0; k < 100; ++k)
        avg_angle += as5048b.angle_rad() / M_PI * 180;

      pc.printf("%d angle %f gain %d lw %d hg %d\n", i, avg_angle/100, gain, diag.low(), diag.high());

      myled = !myled;
      //wait(0.1);
    }
}
